<!DOCTYPE HTML>
<html>
<head>
    <meta  charset="utf-8">
    <title>Changing Time Zones</title>
</head>

<body>
<?php
$UK = new DateTimeZone('Europe/London');
$USeast = new DateTimeZone('America/New_York');
$Bangladesh = new DateTimeZone('Asia/Dhaka');
$Australia = new DateTimeZone('Australia/Sydney');
$now = new DateTime('now', $UK);
?>
<p>In London, it's now <?= $now->format('l, F jS, Y g.ia'); ?>.</p>
<p>In New York, it's <?php
    $now->setTimezone($USeast);
    echo $now->format('l, F jS, Y g.ia'); ?>.</p>
<p>In Bangladesh, it's <?php
    $now->setTimezone($Bangladesh);
    echo $now->format('l, F jS, Y g.ia'); ?>.</p>
	<p>In Australia, it's <?php
    $now->setTimezone($Australia);
    echo $now->format('l, F jS, Y g.ia'); ?>.</p>
</body>
</html>